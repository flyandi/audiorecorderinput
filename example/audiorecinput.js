/** 
  * Audio Recorder Input 
  * Copyright (c) 2013 LawInfo.com, Inc. - a Thomson Reuters Company
  * All rights reserved
  * 
  */
  
/** (Worker Configuration) */
var ARI_WORKER_PATH = "audiorecinput.worker.js";

/** (Constants) */
var ARI_VISUAL_MICROPHONE = 0,
	ARI_ERROR_NOAUDIODEVICE = 0;
	
/** (execute handler) */
(function(window){	

	// verify jQuery
	if(typeof(jQuery)=='undefined') throw('AudioRecorderInput requires jQuery');
	
	/** (AudioRecorderInput) */
	var AudioRecorderInput = (function() {
		
		function AudioRecorderInput(target, settings) {
			// prepare settings
			this.target = target;
			this.settings = $.extend({}, {verbose: false, name: 'audio', loopaudio: false, visualizemic: true, visualization: false, canvas: false}, settings);
			// initialize module
			this.__initialize();
		}

		AudioRecorderInput.prototype = {
			/** (Contexts) */
			ctxaudio: false,
			ctxanalyser: false,
			ctxrecorder: false,
			ctxstreaminput: false,
			
			/** (internals) */
			disabled: false,
			hasvisual: false,
			isrecording: false,
			audiodata: false,
			audioblob: false,
			
			/** (actions) */
			record: function() {
				if(!this.disabled && !this.isrecording) this.__startrecording();
			},
			
			stop: function() {
				if(!this.disabled && this.isrecording) this.__stoprecording();
			},
			
			empty: function() {
				this.audiodata = false;
				this.audioblob = false;			
			},		
			
			/** (audio player) */
			audioplayer: function() {
				if(this.audioblob) return $("<audio></audio>").attr({src: window.URL.createObjectURL(this.audioblob), controls: true});
			},
			
			
			/** (initialize) */
			__initialize: function() {
				// initialize
				var that = this;
				// create input field
				this.inputfield = $("<input/>").attr({name: this.settings.name, type:'hidden'}).appendTo(this.target);
				// init visual
				if(this.settings.visualization && this.settings.canvas) {
					this.hasvisual = true;
				}
				// create context
				window.AudioContext = window.AudioContext || window.webkitAudioContext;
				navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
				window.URL = window.URL || window.webkitURL;
				// start media
				try {
					if(AudioContext && navigator.getUserMedia) {
						// create audio context
						that.ctxaudio = new window.AudioContext;
						// create user media
						navigator.getUserMedia({audio: true}, function(stream) {
							that.__preparestream(stream);						
						}, function(e) {
							// disable
							that.disabled = true;
							// execute error
							that.__error(false, ARI_ERROR_NOAUDIODEVICE)
						});
					}
				} catch(e) {
					that.__error(e, ARI_ERROR_NOAUDIODEVICE);
				}
			},
			
			/** (error) */
			__error: function(e, n) {
				// execute event
				if(typeof(this.settings.onerror)=="function") this.settings.onerror(e, n);
				// check verbose
				if(this.settings.verbose) alert(e);
			},
			
			/** (__process) */
			__process: function() {
				var that = this;
				// animation frame
				window.webkitRequestAnimationFrame(function() {
					that.__process();
				});
				// execute
				this.__render();
			},
			
			/** (__render) */
			__render: function() {
				// verify
				if(!this.hasvisual || !this.isrecording) return;
				// initialize		
				var width = $(this.settings.canvas).width(),
					height = $(this.settings.canvas).height(),
					diameter = width / 2,
					ctx = ($(this.settings.canvas).get(0)).getContext('2d');
				// get sound data
				var freqByteData = new Uint8Array(this.ctxanalyser.frequencyBinCount);
				this.ctxanalyser.getByteFrequencyData(freqByteData);  
				//this.ctxanalyser.getByteTimeDomainData(freqByteData);
				// prepare canvas
				ctx.beginPath();
				ctx.clearRect(0, 0, width, height);
				ctx.fillStyle = '#eeeeee';
				// prepare value
				var op = 0;
				for(var i=0;i<this.ctxanalyser.frequencyBinCount;i++){
					var p = freqByteData[i] / 256;
					if(p > op) op = p;
				}
				op = ((op / diameter) * 10000) - 50;
				if(op > diameter) op = diameter;
				if(op < 10) op = 0;
				// draw
				ctx.arc(diameter, diameter, op, 0, Math.PI*2, true);
				ctx.closePath();
				// complete
				ctx.fill();
				// draw microphone
				if(this.settings.visualizemic) {
					// draw 
					ctx.beginPath();
					ctx.fillStyle = "#000000";
					ctx.arc(diameter, diameter, 50, 0, Math.PI*2, true);
					ctx.closePath();
					ctx.fill();	
					// draw microphone
					var img = new Image();
					img.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAABHCAYAAAB4QnWlAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMS8wNC8xM3twG+YAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzQGstOgAAAHaElEQVRoge2aX0xb1x3Hv8fXfzDhQmyHMCDmT9pQEm9UEdXYVk0oUdVMikrnSqTT3ipVQqrU5mWdCNIyljfGS8gko6iTIqJoZFpfVtqurtqCRFCVpyrULAzZGHDqtEGQhMhg+95zf3vovem9Nwb7gnH6wFc6ks+55/f7ffzzOcdX5xxGRCiWGGMiAD+An5gefQsgQUSPihaMiHZcAHgBHOvv738jFouFyaRYLBbu7+9/A8AxAN6ixNwhsA1AaygUend+fv6zdDq9YobWlE6nV+bn5z8LhULvAmgFYHsq4AAEv9//wtzc3IdEpGwGnEPK3Nzch36//wUAQsnB29vbf55MJm9aADYomUzebG9v7ygpOICGRCJxY7vQmu7cuXMDQENJwAGUT01NDe8UWtPU1NQwgPJdBx8aGjohSVKqWOCSJKUuXbp00iqHzcrSyRhjJ0+e/J3dbi8vyloMwG63l584ceJ1xhizYmcJvLOzc199ff2L1tDyq76+/sXOzs59loys/DyTk5MBWZazxRommmRZzk5OTgZ2bah4PJ59giA4LGWmAAmC4PB4PJYybglcURRuDWn3fFsCB2BpAu2mb6vgPxrtgZdae+Cl1h54qbUHXmrtgZdae+Cl1h54qbUHXmrtgZdadn1F3ZQRdE2ciIq3829B+VgM4K2trd7KysrnBUFgnHNKp9M3AaRKxGpQW1tbeVlZWYfGsra2dgvAivbcAH7t2rVftLS0/E2rz8zM/BbAdOlwf9Dly5efCQQCf9fqsVjsbQAfaXUDeEVFxT5RFJu1uiiKZSWhzCFRFMv0LG6327BhZJiciqJIprpi8reb493g2xzbzGZeVQybMkRkMHY4HLu2IWT27XA4zEkyPDeAc84NDxcXFw3bydXV1and2IZTFIVXV1cbFoFoNKpfUZ5gM4BXVVUZMtzY2HhUX/f5fIuSJMWLg/uDJEmK+3y+RX3bwYMHj2zFZgB3Op3fyrK8odUVRakxxVhXFGWsSLyPpfpc17e5XK7D2mdZltedTudd/XMDeE1NTUyW5cdrZWVl5c/MQdxu93uc86KdEHPO19xu93vmdlEUf6p9lmV5taamZl7/3Dw578myvKBVPB5PwOv1Vpn63GaMjRSBGQDAGLsK4La+zev1Vnk8noBWV5nuGQzNO/2PHj0a1J0UZEZHR1/LcSIgyrJ8qwgnEbeISDT7Hx0dfU2W5YzWT2XKe+p2nHPONaNIJHIdgDtHv8Oc8/9tF5pzfpuIDj8BBLgjkch1XT9ORMcLAUc6nZ7UDDOZzIPe3t6Xc/UjomeI6NNtcIeJqDGXz97e3pczmcwDraPKUtg5pyRJr+qjxOPxzwHUbQJvJ6IeIpqnrc/0FbVPj2qT6/C3Lh6Pf643UlkKPqB1SZI0qXcQDocHARzYpD+IyEVErxDRJSL6mHP+Jef8SyL6WG17Re2T+6QY8ITD4UF9TEmSbmxms9WRXAfnXNI5SY+MjPTmgdcKI6JytbB8/QEcCIVC70iSlNbiqbE3vaSQD+CcPgPZbHbt6tWr5wDUFgBfUAFQOzIy0pvjrsu5Le0KyNw/TT/fxtjY2CCAAICKHQBXAAiMjY0NSpK0YYK+nu+XKiSIjXP+L5NjWlhY+KKvr+9NAEfx/dWmvENCBd4P4GhfX9+bCwsLX5j9qrHyD68CM1RGRKPmILIsZ6LR6H+Gh4f/IIrirwC0ADgEwAdAVItPbWsB0HHx4sWz0Wj0E/0fjE6jaqy8TEzNQqH6o6Iof7bZbIbXXUVR5PX19WQikfgqmUwuxuPxu4lE4iEA+P3+qubm5tq6urrGQ4cOPV9eXl5vPlZXFGXdZrP9BcBfCyYpMOP68ksi+iBHtrarD1Sf1ubINsC18hIRfURED7cB+1C1fWm78a0OlVx6FsCriqL8hoh+LQiCK1cnznmGMTZps9k+AfBvANEdRd1Bxg1lfHx8fyqVWtosxalUaml8fHx/seIVbQuuqakJW11PYoyxpqamYoXbGThjzMkYq2CMHQgGg3U5tjMeS1EUJRgM1jHGDqg2zp3Etufv8gSsiO//cCpPnz7tDwQCtYFAoK6tre2Y0+n0bWbndDp9V65c+dP09PR/Z2ZmkjMzM3cZYwkAawBWyeKF4YImp5qdGgDV58+fP37q1KmOhoaGox6Pp9ntdtfabDarN42UjY2Nu/fv348vLS3dDofDNy9cuPAVgGUA3xFRNq+TrSYAgCoAR7q6uoITExOh5eXl6Ww2W7Q7h5qy2WxqeXl5emJiItTV1RUEcARA1ZZsmwC7ADw3MDDwViQSeT+bza4VG3aLL7EWiUTeHxgYeAvAcwByv48DcANocTgcxwG0AWjr7u4Ozs7Ojunfj0stSZLSs7OzY93d3UGNS2VsAeBmPT09Z86ePft7r9dbzzmXAcDn87W6XK79VsbtbimTyTxYWVmZBQBBEOyrq6vfDA0N/YNtbGzcKysrq37agFaUTqeXGVEBy8qPUDbO+dfY3X3vYos451/bBEE4AyDytGksKCIIwpn/A2g/6ved1er0AAAAAElFTkSuQmCC";
					ctx.drawImage(img, diameter - (img.width / 2), diameter - (img.height / 2));
				}
			},
			
			/** (__preparestream) */
			__preparestream: function(stream) {
				var that = this;
				// assign context
				this.ctxstreaminput = this.ctxaudio.createMediaStreamSource(stream);
				// check destination
				if(this.settings.loopaudio) {
					thix.ctxstreaminput.connect(this.ctxaudio.destination);
				}
				// create analyser
				this.ctxanalyser = this.ctxaudio.createAnalyser();
				// create recorder
				this.ctxrecorder = new AudioRecorder(this.ctxstreaminput);
				// connect
				this.ctxstreaminput.connect(this.ctxanalyser);
				// start process
				this.__process();
			},
			
			/** (__startrecording) */
			__startrecording: function() {
				// precheck
				if(this.ctxrecorder) {
					// set flag
					this.isrecording = true;
					// execute event
					if(typeof(this.settings.onrecord)=="function") this.settings.onrecord();
					// start recorder
					this.ctxrecorder.record();
				}
			},
			
			/** (__stoprecording) */
			__stoprecording: function() {
				// precheck
				if(this.ctxrecorder) {
					// set flag
					this.isrecording = false;				
					// execute event
					if(typeof(this.settings.onstop)=="function") this.settings.onstop();
					// stop
					this.ctxrecorder.stop();
					// create blob data
					this.__processblobdata();
					// clear recorder
					this.ctxrecorder.clear();
				}
			},
			
			/** (__processblobdata) */
			__processblobdata: function() {
				if(this.ctxrecorder) {
					var that = this;
					// get blob data
					this.ctxrecorder.blobdata(function(blob) {
						// assign blob
						that.audioblob = blob;
						// create reader
						var reader = new window.FileReader();
						reader.readAsDataURL(blob);	
						// read audio data
						reader.onloadend = function() {
							// assign audio data
							that.audiodata = reader.result;
							// Update input field
							that.inputfield.val(that.audiodata);
							// execute event
							if(typeof(that.settings.ondata)=="function") that.settings.ondata(that.audiodata, blob);
						};
					});
				}
			}
		};
		
		return AudioRecorderInput;
	})();
	
	
	/** (AudioRecorder) */
	var AudioRecorder = function(source, settings) {
		// initialize
		var settings = settings || {},
			bufferlength = settings.bufferlength || 4096;
		// initialize nodes
		this.context = source.context;
		this.node = this.context.createJavaScriptNode(bufferlength, 2, 2);
		// create worker
		var worker = new Worker(settings.workerpath || ARI_WORKER_PATH);
		// initialize worker
		worker.postMessage({
			command: 'init',
			config: {
				sampleRate: this.context.sampleRate
			}
		});
		// initialize handlers and callback
		var recording = false,
			currentcallback = false;
			
		// Audio Processing
		this.node.onaudioprocess = function(e){
			// verify
			if (!recording) return;
			// post to worker
			worker.postMessage({
				command: 'record',
				buffer: [
					e.inputBuffer.getChannelData(0),
					e.inputBuffer.getChannelData(1)
				]
			});
		}
		
		// Configuration
		this.configure = function(cfg){
			for (var prop in cfg) {
				if (cfg.hasOwnProperty(prop)) {
					config[prop] = cfg[prop];
				}
			}
		}
		
		// Record
		this.record = function(){
			recording = true;
		}

		// Stop
		this.stop = function(){
			recording = false;
		}

		// Clear
		this.clear = function(){
			worker.postMessage({ command: 'clear' });
		}
		
		// Buffer Management
		this.getBuffer = function(cb) {
			currentcallback = cb || config.callback;
			worker.postMessage({ command: 'getBuffer' })
		}

		// Blob Data Management
		this.blobdata = function(cb, type){
			// initialize
			currentcallback = cb || settings.callback;
			type = type || settings.type || 'audio/wav';
			// check callback
			if (!currentcallback) throw new Error('AudioRecorder: Callback not set');
			// get blob
			worker.postMessage({
				command: 'getBlobData',
				type: type
			});
		}
		
		// Event Handling
		worker.onmessage = function(e){
			var blob = e.data;
			currentcallback(blob);
		}
		
		// Connect source to node
		source.connect(this.node);
		this.node.connect(this.context.destination);    //this should not be necessary
  };

  // assign
  window.AudioRecorderInput = AudioRecorderInput;
  window.AudioRecorder = AudioRecorder;

})(window);